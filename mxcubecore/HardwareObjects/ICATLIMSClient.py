"""
A client for PyISPyB Webservices.
"""
import json
import logging
import pathlib
import shutil
import time

from mxcubecore.BaseHardwareObjects import HardwareObject
from mxcubecore import HardwareRepository as HWR
from pyicat_plus.client.main import IcatClient, IcatInvestigationClient

### This should go to configuration file
LOGIN_TYPE_FALLBACK = "user"
metadata_urls = ["bcu-mq-01.esrf.fr:61613", "bcu-mq-02.esrf.fr:61613"]
elogbook_url="https://icatplus.esrf.fr"

class ICATLIMSClient(HardwareObject):
    """
    ICAT client.
    """

    def __init__(self, name):
        super().__init__(name)
        self.beamline_name = HWR.beamline.session.beamline_name

        self.icatClient = IcatClient(metadata_urls=metadata_urls, elogbook_url=elogbook_url)

        self.__test_proposal = {
            "status": {"code": "ok"},
            "Person": {
                "personId": 1,
                "laboratoryId": 1,
                "login": None,
                "familyName": "operator on IDTESTeh1",
            },
            "Proposal": {
                "code": "idtest",
                "title": "operator on IDTESTeh1",
                "personId": 1,
                "number": "0",
                "proposalId": 1,
                "type": "MX",
            },
            "Session":[
                {
                    "scheduled": 0,
                    "startDate": "2013-06-11 00:00:00",
                    "endDate": "2023-06-12 07:59:59",
                    "beamlineName": self.beamline_name,
                    "timeStamp": "2013-06-11 09:40:36",
                    "comments": "Session created by the BCM",
                    "sessionId": 34591,
                    "proposalId": 1,
                    "nbShifts": 3,
                }]
            ,
            "Laboratory": {"laboratoryId": 1, "name": "TEST eh1"},
        }

    @property
    def loginType(self):
        return self.get_property("loginType", LOGIN_TYPE_FALLBACK)


    def login(self, loginID, psd, ldap_connection=None, create_session=True):

        # TODO: Login into ICAT and get the token
        logging.getLogger("MX3.HWR").error(
                "[ICATClient] Do login %s %s is missing" % (loginID, psd)
            )
        import pdb
        pdb.set_trace()
        # TODO: Get the investigations associated to the user

        return {
            "status": {"code": "ok", "msg": "Successful login"},
            "Proposal": {'code': 'idtest', 'title': 'operator on IDTESTeh1', 'personId': 1, 'number': '0', 'proposalId': 1, 'type': 'MX'},
            "Session": {
                "session": {'scheduled': 0, 'startDate': '2013-06-11 00:00:00', 'endDate': '2023-06-12 07:59:59', 'beamlineName': 'mxcubewebtest', 'timeStamp': '2013-06-11 09:40:36', 'comments': 'Session created by the BCM', 'sessionId': 34591, 'proposalId': 1, 'nbShifts': 3},
                "new_session_flag": False,
                "is_inhouse": False,
            },
            "local_contact": "BL Scientist",
            "Person": {'personId': 1, 'laboratoryId': 1, 'login': "test Login", 'familyName': 'operator on IDTESTeh1'},
            "Laboratory":{'laboratoryId': 1, 'name': 'TEST eh1'}
        }

    def create_session(self, session_dict):
        pass

    def get_todays_session(self, prop, create_session=True):
        return {
            'session':
                {'proposalId': 1, 'startDate': '2023-12-21 00:00:00', 'endDate': '2023-12-22 07:59:59', 'beamlineName': 'mxcubewebtest', 'scheduled': 0, 'nbShifts': 3, 'comments': 'Session created by the BCM', 'sessionId': None},
                'new_session_flag': True,
                'is_inhouse': True
                }

    def echo(self):
        """Mockup for the echo method."""
        return True

    def get_proposal(self, proposal_code, proposal_number):
        return self.__test_proposal

    def get_proposals_by_user(self, user_name):
        return [self.__test_proposal,self.__test_proposal]

    def get_session_local_contact(self, session_id):
        return {
            "personId": 1,
            "laboratoryId": 1,
            "login": None,
            "familyName": "operator on ID14eh1",
        }

    def translate(self, code, what):
        """
        Given a proposal code, returns the correct code to use in the GUI,
        or what to send to LDAP, user office database, or the ISPyB database.
        """
        try:
            translated = self.__translations[code][what]
        except KeyError:
            translated = code

        return translated

    def is_connected(self):
        return self.login_ok

    def isInhouseUser(self, proposal_code, proposal_number):
        """
        Returns True if the proposal is considered to be a
        in-house user.

        :param proposal_code:
        :type proposal_code: str

        :param proposal_number:
        :type proposal_number: str

        :rtype: bool
        """
        for proposal in self["inhouse"]:
            if proposal_code == proposal.code:
                if str(proposal_number) == str(proposal.number):
                    return True
        return False

    def store_data_collection(self, mx_collection, bl_config=None):
        """
        Stores the data collection mx_collection, and the beamline setup
        if provided.

        :param mx_collection: The data collection parameters.
        :type mx_collection: dict

        :param bl_config: The beamline setup.
        :type bl_config: dict

        :returns: None

        """
        logging.getLogger("HWR").debug(
            "Data collection parameters stored " + "in ISPyB: %s" % str(mx_collection)
        )
        logging.getLogger("HWR").debug(
            "Beamline setup stored in ISPyB: %s" % str(bl_config)
        )

        return None, None

    def store_beamline_setup(self, session_id, bl_config):
        """
        Stores the beamline setup dict <bl_config>.

        :param session_id: The session id that the bl_config
                           should be associated with.
        :type session_id: int

        :param bl_config: The dictonary with beamline settings.
        :type bl_config: dict

        :returns beamline_setup_id: The database id of the beamline setup.
        :rtype: str
        """
        pass

    def update_data_collection(self, mx_collection, wait=False):
        """
        Updates the datacollction mx_collection, this requires that the
        collectionId attribute is set and exists in the database.

        :param mx_collection: The dictionary with collections parameters.
        :type mx_collection: dict

        :returns: None
        """
        pass

    def update_bl_sample(self, bl_sample):
        """
        Creates or stos a BLSample entry.

        :param sample_dict: A dictonary with the properties for the entry.
        :type sample_dict: dict
        # NBNB update doc string
        """
        pass

    def store_image(self, image_dict):
        """
        Stores the image (image parameters) <image_dict>

        :param image_dict: A dictonary with image pramaters.
        :type image_dict: dict

        :returns: None
        """
        pass

    def __find_sample(self, sample_ref_list, code=None, location=None):
        """
        Returns the sample with the matching "search criteria" <code> and/or
        <location> with-in the list sample_ref_list.

        The sample_ref object is defined in the head of the file.

        :param sample_ref_list: The list of sample_refs to search.

        :param code: The vial datamatrix code (or bar code)

        :param location: A tuple (<basket>, <vial>) to search for.
        :type location: tuple
        """
        pass

    def get_samples(self, proposal_id, session_id):

        # Try GPhL emulation samples, if available
        gphl_workflow = HWR.beamline.gphl_workflow
        if gphl_workflow is not None:
            sample_dicts = gphl_workflow.get_emulation_samples()
            if sample_dicts:
                return sample_dicts

        return [
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "1",
                "crystalSpaceGroup": "P212121",
                "diffractionPlan": {
                    "diffractionPlanId": 457980,
                    "experimentKind": "Default",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "Default",
                "proteinAcronym": "A-TIM",
                "sampleId": 515485,
                "sampleLocation": "1",
                "sampleName": "fghfg",
                "smiles": None,
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457833,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515419,
                "sampleLocation": "1",
                "sampleName": "sample",
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457834,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515420,
                "sampleLocation": "2",
                "sampleName": "sample",
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457835,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515421,
                "sampleLocation": "3",
                "sampleName": "sample",
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457836,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515422,
                "sampleLocation": "5",
                "sampleName": "sample",
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457837,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515423,
                "sampleLocation": "6",
                "sampleName": "sample",
            },
            {
                "cellA": 0.0,
                "cellAlpha": 0.0,
                "cellB": 0.0,
                "cellBeta": 0.0,
                "cellC": 0.0,
                "cellGamma": 0.0,
                "containerSampleChangerLocation": "2",
                "crystalSpaceGroup": "P2",
                "diffractionPlan": {
                    "diffractionPlanId": 457838,
                    "experimentKind": "OSC",
                    "numberOfPositions": 0,
                    "observedResolution": 0.0,
                    "preferredBeamDiameter": 0.0,
                    "radiationSensitivity": 0.0,
                    "requiredCompleteness": 0.0,
                    "requiredMultiplicity": 0.0,
                    "requiredResolution": 0.0,
                },
                "experimentType": "OSC",
                "proteinAcronym": "B2 hexa",
                "sampleId": 515424,
                "sampleLocation": "7",
                "sampleName": "sample",
            },
        ]

    def create_mx_collection(self, collection_parameters):
        try:
            fileinfo = collection_parameters["fileinfo"]
            directory = pathlib.Path(fileinfo["directory"])
            dataset_name = directory.name
            # Determine the scan type
            if dataset_name.endswith("mesh"):
                scanType = "mesh"
            elif dataset_name.endswith("line"):
                scanType = "line"
            elif dataset_name.endswith("characterisation"):
                scanType = "characterisation"
            elif dataset_name.endswith("datacollection"):
                scanType = "datacollection"
            else:
                scanType = collection_parameters["experiment_type"]
            workflow_type = collection_parameters.get("workflow_type")
            if workflow_type is None:
                if directory.name.startswith("run"):
                    sample_name = directory.parent.name
                else:
                    sample_name = directory.name
                    dataset_name = fileinfo["prefix"]
            else:
                sample_name = directory.parent.parent.name
            oscillation_sequence = collection_parameters["oscillation_sequence"][0]
            beamline = HWR.beamline.session.beamline_name.lower()
            distance = HWR.beamline.detector.distance.get_value()
            proposal = f"{HWR.beamline.session.proposal_code}{HWR.beamline.session.proposal_number}"
            metadata = {
                "MX_beamShape": collection_parameters["beamShape"],
                "MX_beamSizeAtSampleX": collection_parameters["beamSizeAtSampleX"],
                "MX_beamSizeAtSampleY": collection_parameters["beamSizeAtSampleY"],
                "MX_dataCollectionId": collection_parameters["collection_id"],
                "MX_detectorDistance": distance,
                "MX_directory": str(directory),
                "MX_exposureTime": oscillation_sequence["exposure_time"],
                "MX_flux": collection_parameters["flux"],
                "MX_fluxEnd": collection_parameters["flux_end"],
                "MX_numberOfImages": oscillation_sequence["number_of_images"],
                "MX_oscillationRange": oscillation_sequence["range"],
                "MX_oscillationStart": oscillation_sequence["start"],
                "MX_oscillationOverlap": oscillation_sequence["overlap"],
                "MX_resolution": collection_parameters["resolution"],
                "scanType": scanType,
                "MX_startImageNumber": oscillation_sequence["start_image_number"],
                "MX_template": fileinfo["template"],
                "MX_transmission": collection_parameters["transmission"],
                "MX_xBeam": collection_parameters["xBeam"],
                "MX_yBeam": collection_parameters["yBeam"],
                "Sample_name": sample_name,
                "InstrumentMonochromator_wavelength": collection_parameters[
                    "wavelength"
                ],
                "Workflow_name": collection_parameters.get("workflow_name", None),
                "Workflow_type": collection_parameters.get("workflow_type", None),
                "Workflow_id": collection_parameters.get("workflow_uid", None),
            }
            # Store metadata on disk
            icat_metadata_path = pathlib.Path(directory) / "metadata.json"
            with open(icat_metadata_path, "w") as f:
                f.write(json.dumps(metadata, indent=4))
            # Create ICAT gallery
            gallery_path = directory / "gallery"
            gallery_path.mkdir(mode=0o755, exist_ok=True)
            for snapshot_index in range(1, 5):
                key = f"xtalSnapshotFullPath{snapshot_index}"
                if key in collection_parameters:
                    snapshot_path = pathlib.Path(collection_parameters[key])
                    if snapshot_path.exists():
                        logging.getLogger("HWR").debug(
                            f"Copying snapshot index {snapshot_index} to gallery"
                        )
                        shutil.copy(snapshot_path, gallery_path)
            logging.getLogger("HWR").info(f"Beamline: {beamline}")
            logging.getLogger("HWR").info(f"Proposal: {proposal}")

            self.icatClient.store_dataset(
                beamline=beamline,
                proposal=proposal,
                dataset=dataset_name,
                path=str(directory),
                metadata=metadata,
            )
            logging.getLogger("HWR").debug("Done uploading to ICAT")
        except Exception as e:
            logging.getLogger("HWR").exception(e)

    def create_ssx_collection(
        self, data_path, collection_parameters, beamline_parameters, extra_lims_values
    ):
        try:
            data = {
                "MX_scanType": "SSX-Jet",
                "MX_beamShape": beamline_parameters.beam_shape,
                "MX_beamSizeAtSampleX": beamline_parameters.beam_size_x,
                "MX_beamSizeAtSampleY": beamline_parameters.beam_size_y,
                "MX_detectorDistance": beamline_parameters.detector_distance,
                "MX_directory": data_path,
                "MX_exposureTime": collection_parameters.user_collection_parameters.exp_time,
                "MX_flux": extra_lims_values.flux_start,
                "MX_fluxEnd": extra_lims_values.flux_end,
                "MX_numberOfImages": collection_parameters.collection_parameters.num_images,
                "MX_resolution": beamline_parameters.resolution,
                "MX_transmission": beamline_parameters.transmission,
                "MX_xBeam": beamline_parameters.beam_x,
                "MX_yBeam": beamline_parameters.beam_y,
                "Sample_name": collection_parameters.path_parameters.prefix,
                "InstrumentMonochromator_wavelength": beamline_parameters.wavelength,
            }



            self.icatClient.store_dataset(
                beamline="ID29",
                proposal=f"{HWR.beamline.session.proposal_code}{HWR.beamline.session.proposal_number}",
                dataset=collection_parameters.path_parameters.prefix,
                path=data_path,
                metadata=data,
            )
        except Exception:
            logging.getLogger("HWR").exception("")
